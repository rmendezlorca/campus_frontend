import { TokenComponent } from './pages/login/recuperar/token/token.component';
import { HomeCursoComponent } from './pages/home-curso/home-curso.component';
import { RecuperarComponent } from './pages/login/recuperar/recuperar.component';
import { Not403Component } from './pages/not403/not403.component';
import { LoginComponent } from './pages/login/login.component';
import { RegistrarComponent } from './pages/registrar/registrar.component';
import { GuardService } from './_service/guard.service';
import { MyAppComponent } from './pages/my-app/my-app.component';
import { EstacionamientoComponent } from './pages/estacionamiento/estacionamiento.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SilabusComponent } from './pages/silabus/silabus.component';
import { VideoClassComponent } from './pages/videoclass/videoclass.component';

const routes: Routes = [
  //{path:'estacionamiento',component:EstacionamientoComponent, canActivate:[GuardService]},
    /*{ path:'videotest',component:MyAppComponent , children:[{
      path:'videolist/:id',component:MyAppComponent
    }],outlet:'classroom'},*/
  //
  {path:'registrar',component:RegistrarComponent},
  {path:'login',component:LoginComponent},
  {path:'not-403',component:Not403Component},
  {path:'',redirectTo:'registrar',pathMatch:'full'},
  { path: 'recuperar', component: RecuperarComponent, children:[{
    path:':token',component:TokenComponent
  }]},
  { path: 'home-cursos', component: HomeCursoComponent },
  { path: 'silabus', component: SilabusComponent},
  {
    path:'video_class/:id',component:VideoClassComponent
  },  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
