import { ContenidoCurso } from './contenidocurso';
export class Curso{    
    duracion : number;
    id : number;
    nombre : string;
    contenidocurso: ContenidoCurso[];
}