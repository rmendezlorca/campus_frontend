import { CursoService } from './../../_service/curso.service';
import { PATH_VIDEO } from './../../_shared/var.constant';
import { ContenidoCurso } from './../../_model/contenidocurso';
import { Curso } from './../../_model/curso';

import { Router, ActivatedRoute, Params } from '@angular/router';

import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { VgAPI } from 'videogular2/core';

export interface IMedia {
  title: string;
  src: string;
  type: string;
}

@Component({
  selector: 'app-videoclass',
  templateUrl: './videoclass.component.html',
  styleUrls: ['./videoclass.component.css']
})
export class VideoClassComponent implements OnInit {
    api:VgAPI;
    cursos:Curso[];
    contenidocurso:ContenidoCurso[];
    //playlist:Array<IMedia>=[];
    id:number;
    src1:string=PATH_VIDEO;
    playset:Array<Object>;
    playlist:Array<Object>;
    currentIndex=0;
    contenidos:ContenidoCurso[];
    currentItem:Object={src:"",title:"",type:"video/mp4"};
    constructor(private cdRef:ChangeDetectorRef,private cursoService:CursoService,private route:Router,private router:ActivatedRoute) {      
      
      this.router.params.subscribe((params: Params)=>{
        this.id=params['id'];
      });
      /*this.cursoService.contenidoCambio.subscribe(data=>{
        this.contenidos=data;
      })*/
      this.cursoService.buscarContenidoCursoById(this.id).subscribe(data=>{
        //this.contenidos.push(data);
        this.currentItem={title:'a',            
        src:this.src1.concat('/'+data.resource),
        type:"video/mp4"};  
        this.playlist=[{title:data.descripcion,            
        src:this.src1.concat('/'+data.resource),
        type:"video/mp4"}];
        
        this.playset=[{title:'a',            
        src:this.src1.concat('/'+data.resource),
        type:"video/mp4"}];

          
      });
      /*this.router.params.subscribe((params: Params)=>{
        this.id=params['id'];  
        
      });            */
    }

    ngOnInit() {
     /*   this.cursoService.buscarContenidoCursoById(5).subscribe(data=>{
          this.currentItem={title:'a',            
          src:this.src1.concat('/'+data.resource),
          type:"video/mp4"};  
          this.playlist=[{title:data.descripcion,            
          src:this.src1.concat('/'+data.resource),
          type:"video/mp4"}];
          
          this.playset=[{title:'a',            
          src:this.src1.concat('/'+data.resource),
          type:"video/mp4"}];

            
        });*/
      };
  ngAfterViewChecked()
  {
  this.cdRef.detectChanges();
  }  
  

onClickPlaylistItem(item: IMedia) {
    this.currentIndex = 0;
    this.currentItem = item;
}
nextVideo() {
  this.currentIndex++;

  if (this.currentIndex === this.playset.length) {
      this.currentIndex = 0;
  }

  this.currentItem = this.playset[ this.currentIndex ];
}
onPlayerReady(api: VgAPI) {
  this.api = api;
  this.api.getDefaultMedia().subscriptions.loadedMetadata.subscribe(
    this.playVideo.bind(this)
 );
 this.api.getDefaultMedia().subscriptions.ended.subscribe(
    this.nextVideo.bind(this)
 );
}
playVideo() {
  this.api.play();
}
}
