import { Curso } from './../../_model/curso';
import { CursoService } from './../../_service/curso.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

export interface Tile {
  //color: string;
  cols: number;
  rows: number;
  text: string
}
@Component({
  selector: 'app-home-curso',
  templateUrl: './home-curso.component.html',
  styleUrls: ['./home-curso.component.css']
})
export class HomeCursoComponent implements OnInit {
  tiles: Tile[] = [
    {text: 'One', cols: 1, rows: 2},
    {text: 'Two', cols: 1, rows: 2},
    {text: 'Three', cols: 1, rows: 2,},
    {text: 'Four', cols: 1, rows: 2},
    {text: 'One', cols: 1, rows: 2},
    {text: 'Two', cols: 1, rows: 2},
    {text: 'Three', cols: 1, rows: 2},
    {text: 'Four', cols: 1, rows: 2},
  ];
  cursos:Curso[];
  constructor(private router: Router,private cursoService:CursoService) { }

  ngOnInit() {
    let self = this;
    try {
      this.cursoService.listar()
        .subscribe(data=>{
          console.log(data);
          //this.cursoService.cursoCambio.next(data);
          self.cursos = data;
        });
    }
    catch(error) {
      console.error(error);
      this.router.navigate(["login"]);
    }
    
  }
  showCurso(curso:Curso){
    sessionStorage.setItem('cursoActual', JSON.stringify(curso) );
    this.router.navigate(["silabus"]);
  }

}
