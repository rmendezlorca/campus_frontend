import { PATH_VIDEO } from './../../_shared/var.constant';
import { ContenidoCurso } from './../../_model/contenidocurso';
import { CursoService } from './../../_service/curso.service';
import { Curso } from './../../_model/curso';
import { MenuService } from 'src/app/_service/menu.service';
import { Menu } from './../../_model/menu';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { VgAPI } from 'videogular2/core';
export interface IMedia {
  title: string;
  src: string;
  type: string;
}
export interface Recurso {
  titulo :string,
  url : string,
}
@Component({
  selector: 'app-silabus',
  templateUrl: './silabus.component.html',
  styleUrls: ['./silabus.component.css']
})
export class SilabusComponent implements OnInit {

  curso:Curso;
  contenidos:ContenidoCurso[];
  recursos  = [];
  api:VgAPI;
  contenidocurso:ContenidoCurso[];
    //playlist:Array<IMedia>=[];
  id:number;
  src1:string=PATH_VIDEO;
  playset:Array<Object>;
  playlist:Array<Object>;
  currentIndex=0;
  currentItem:Object={src:"",title:"",type:"video/mp4"};

  constructor(private router: Router,private cursoService:CursoService,private cdRef:ChangeDetectorRef) {
    let cursoActual : Curso = JSON.parse(sessionStorage.getItem("cursoActual"));
    this.curso = cursoActual;
    this.contenidos = cursoActual.contenidocurso;
    if( cursoActual.contenidocurso.length > 0 ){
      this.click(cursoActual.contenidocurso[0].id);
    }else{
      console.log(cursoActual);
      console.warn("no hay contenido en este curso");
    }
    let recurso = { titulo : 'documento pdf' , url:'https://ocw.innova.uned.es/mm2/tm/contenidos/pdf/tema4/tmm_tema4_video_digital_presentacion.pdf'};
    console.log(this.recursos);
    this.recursos.push(recurso);
  }

  ngOnInit() {

    }

    goTo(url: string){
        window.open(url, "_blank");
    }
//***video**//
click(id:number){
  this.cursoService.buscarContenidoCursoById(id).subscribe(data=>{
    this.currentItem={title:'a',
    src:this.src1.concat('/'+data.resource),
    type:"video/mp4"};
    this.playlist=[{title:data.descripcion,
    src:this.src1.concat('/'+data.resource),
    type:"video/mp4"}];

    this.playset=[{title:'a',
    src:this.src1.concat('/'+data.resource),
    type:"video/mp4"}];


  });
}


    ngAfterViewChecked()
    {
      this.cdRef.detectChanges();
    }


    onClickPlaylistItem(item: IMedia) {
      this.currentIndex = 0;
      this.currentItem = item;
    }
    nextVideo() {
      this.currentIndex++;

      if (this.currentIndex === this.playset.length) {
        this.currentIndex = 0;
      }

      this.currentItem = this.playset[ this.currentIndex ];
    }
    onPlayerReady(api: VgAPI) {
      this.api = api;
      this.api.getDefaultMedia().subscriptions.loadedMetadata.subscribe(
      this.playVideo.bind(this)
      );
      this.api.getDefaultMedia().subscriptions.ended.subscribe(
      this.nextVideo.bind(this)
      );
    }
    playVideo() {
      this.api.play();
    }
  }
