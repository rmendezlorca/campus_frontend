import { catchError } from 'rxjs/operators';
import { TOKEN_NAME } from './../../_shared/var.constant';
import { LoginService } from './../../_service/login.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import '../login-animation.js';
import { MenuService } from 'src/app/_service/menu.service';
import * as decode from 'jwt-decode';
import { throwError } from 'rxjs';


@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {

  usuario: string;
  clave: string;
  mensaje: string = "";
  error: string = "";
  iniciando : boolean = false;
  constructor(private loginService: LoginService,private router: Router,private menuService: MenuService){}
  //constructor(private loginService: LoginService, private menuService: MenuService, private router: Router) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    (window as any).initialize();
  }
  registrar(){
    //console.log(this.usuario);
    //console.log(this.clave);
    try{
      this.loginService.registrar(this.usuario, this.clave).subscribe(data => {
        if (data) {
          console.log(data);
          this.router.navigate(["login"]);
        }else{
          this.mensaje = "algo ha salido mal";
        }
      });
    }
    catch(error){
      this.iniciando = false;
      this.mensaje = "algo ha salido mal";
    }
}



}
