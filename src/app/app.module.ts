import { VgAPI } from 'videogular2/core';
import { VgOverlayPlayModule } from 'videogular2/overlay-play';
import { VgControlsModule } from 'videogular2/controls';
import { VgCoreModule } from 'videogular2/core';
import {VgBufferingModule} from 'videogular2/buffering';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from './material/material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EstacionamientoComponent } from './pages/estacionamiento/estacionamiento.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MyAppComponent } from './pages/my-app/my-app.component';
import { LoginComponent } from './pages/login/login.component';
import { RegistrarComponent } from './pages/registrar/registrar.component';

import { Not403Component } from './pages/not403/not403.component';

import { RecuperarComponent } from './pages/login/recuperar/recuperar.component';
import { HomeCursoComponent } from './pages/home-curso/home-curso.component';
import { SilabusComponent } from './pages/silabus/silabus.component';
import { VideoClassComponent } from './pages/videoclass/videoclass.component';
import { TokenComponent } from './pages/login/recuperar/token/token.component';

@NgModule({
  declarations: [
    AppComponent,
    EstacionamientoComponent,
    MyAppComponent,
    LoginComponent,
    RegistrarComponent,
    Not403Component,
    RecuperarComponent,
    HomeCursoComponent,
    SilabusComponent,
    VideoClassComponent,
    TokenComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    VgCoreModule


  ],
  providers: [VgAPI
  /*  ,{provide: HTTP_INTERCEPTORS,
    useClass: ServerErrorsInterceptor,
    multi: true,}*/
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
