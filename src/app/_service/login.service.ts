import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { HOST, TOKEN_AUTH_USERNAME, TOKEN_AUTH_PASSWORD, TOKEN_NAME } from './../_shared/var.constant';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url:string=`${HOST}/oauth/token`;
  constructor(private http: HttpClient, private router: Router) { }
  user:string;
  login2(usuario: string, contrasena: string){
    const body = `grant_type=password&username=${encodeURIComponent(usuario)}&password=${encodeURIComponent(contrasena)}`;
    //this.user=usuario;
    return this.http.post(this.url, body, {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8').set('Authorization', 'Basic ' + btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD))
    })

  }
  login(usuario: string, contrasena: string){
    const body = `grant_type=password&username=${encodeURIComponent(usuario)}&password=${encodeURIComponent(contrasena)}`;
    return this.http.post(this.url,body,{
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8').set('Authorization', 'Basic ' + btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD))
    })/*.pipe(
      catchError(error=>{
        console.log('Handling error locally and rethrowing it...', error);
        return throwError(error);
      })
  ).subscribe(res=>console.log('Http Response ',res),
  err=>console.log('Http Response ',err),
  ()=>console.log('Http request Complete')
  );    */

  }

  enviarCorreo(correo: string) {
    return this.http.post<number>(`${HOST}/login/enviarCorreo`, correo, {
      headers: new HttpHeaders().set('Content-Type', 'text/plain')
    });
  }

  verificarTokenReset(token: string) {
    return this.http.get<number>(`${HOST}/login/restablecer/verificar/${token}`);
  }

  restablecer(token: string, clave: string) {
    return this.http.post<number>(`${HOST}/login/restablecer/${token}`, clave, {
      headers: new HttpHeaders().set('Content-Type', 'text/plain')
    });
  }
  registrar(usuario : string,clave : string){    
    return this.http.post<number>(`${HOST}/usuarios/crear/${usuario}/${clave}`, {
      headers: new HttpHeaders().set('Content-Type', 'text/plain')
    });
  }
  cerrarSesion(){
    sessionStorage.clear();
    this.router.navigate(['login']);
  }
  estadoLogeado(){
      let token =sessionStorage.getItem(TOKEN_NAME);
      return token!=null;
  }
}
