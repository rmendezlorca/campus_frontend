import { HttpClient } from '@angular/common/http';
import { UsuarioCurso } from './../_model/usuariocurso';
import { HOST } from './../_shared/var.constant';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuarioCursoService {
  usuarioCursoCambio = new Subject<UsuarioCurso[]>();
  //contenidoCambio=new Subject<ContenidoCurso[]>();
  private url: string = `${HOST}`;
  constructor(private http: HttpClient) { }
}
