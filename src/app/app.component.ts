import { LoginService } from './_service/login.service';
import { Menu } from './_model/menu';
import { MenuService } from './_service/menu.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'carservice-app';
  menus:Menu[]=[]
  constructor(private router : Router,private menuService:MenuService,public loginService:LoginService){}
  ngOnInit(){
    if(this.loginService.estadoLogeado()){
      this.menuService.listar().subscribe(data=>{
        console.log("data menuu",data);
        this.menus = data;
      })
    }
    /*this.menuService.menuCambio.subscribe(data=>{
      console.log("data menu",data);
      this.menus=data;
    });
    */
  }
  home(){
    this.router.navigate(["home-cursos"]);
  }
}
